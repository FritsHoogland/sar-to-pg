-- cpu
-- linux format: hostname;interval;timestamp;CPU;%user;%nice;%system;%iowait;%steal;%idle
drop table if exists sar_cpu;
create table sar_cpu ( hostname varchar(100), interval int, timestamp timestamp, cpu numeric, perc_user numeric, perc_nice numeric, perc_system numeric, perc_iowait numeric, perc_steal numeric, perc_idle numeric );

-- restart
drop table if exists sar_restart;
create table sar_restart ( hostname varchar(100), interval int, timestamp timestamp, message varchar(100) );

-- blocks read
-- linux format: hostname;interval;timestamp;tps;rtps;wtps;bread/s;bwrtn/s
drop table if exists sar_blocks_read;
create table sar_blocks_read ( hostname varchar(100), interval int, timestamp timestamp, tps numeric, rtps numeric, wtps numeric, bread_s numeric, bwrtn_s numeric );

-- disk
-- linux format: hostname;interval;timestamp;DEV;tps;rd_sec/s;wr_sec/s;avgrq-sz;avgqu-sz;await;svctm;%util
drop table if exists sar_disk;
create table sar_disk ( hostname varchar(100), interval int, timestamp timestamp, dev varchar(50), tps numeric, rd_sec_s numeric, wr_sec_s numeric, avgrq_sz numeric, avgqu_sz numeric, await numeric, svctm numeric, perc_util numeric );

-- hugepages
-- linux format: hostname;interval;timestamp;kbhugfree;kbhugused;%hugused
drop table if exists sar_hugepages;
create table sar_hugepages ( hostname varchar(100), interval int, timestamp timestamp, kbhugfree numeric, kbhugused numeric, perc_hugused numeric );

-- kernel tables
-- linux format: hostname;interval;timestamp;dentunusd;file-nr;inode-nr;pty-nr
drop table if exists sar_kernel_tables;
create table sar_kernel_tables ( hostname varchar(100), interval int, timestamp timestamp, dentunusd numeric, file_nr numeric, inode_nr numeric, pty_nr numeric );

-- memory allocation
-- linux format: hostname;interval;timestamp;frmpg/s;bufpg/s;campg/s
drop table if exists sar_memory_allocation;
create table sar_memory_allocation ( hostname varchar(100), interval int, timestamp timestamp, frmpg_s numeric, bufpg_s numeric, campg_s numeric );

-- memory utilization
-- linux format: hostname;interval;timestamp;kbmemfree;kbmemused;%memused;kbbuffers;kbcached;kbcommit;%commit;kbactive;kbinact;kbdirty
drop table if exists sar_memory_utilization;
create table sar_memory_utilization ( hostname varchar(100), interval int, timestamp timestamp, kbmemfree numeric, kbmemused numeric, perc_memused numeric, kbbuffers numeric, kbcached numeric, kbcommit numeric, perc_commit numeric, kbactive numeric, kbinact numeric, kbdirty numeric );

-- network device
-- linux format: hostname;interval;timestamp;IFACE;rxpck/s;txpck/s;rxkB/s;txkB/s;rxcmp/s;txcmp/s;rxmcst/s
drop table if exists sar_network_device;
create table sar_network_device ( hostname varchar(100), interval int, timestamp timestamp, interface varchar(50), rxpck_s numeric, txpck_s numeric, rxkB_s numeric, txkB_s numeric, rxcmp_s numeric, txcmp_s numeric, rxmcst_s numeric );

-- network device errors
-- linux format: hostname;interval;timestamp;IFACE;rxerr/s;txerr/s;coll/s;rxdrop/s;txdrop/s;txcarr/s;rxfram/s;rxfifo/s;txfifo/s
drop table if exists sar_network_device_errors;
create table sar_network_device_errors ( hostname varchar(100), interval int, timestamp timestamp, interface varchar(50), rxerr_s numeric, txerr_s numeric, coll_s numeric, rxdrop_s numeric, txdrop_s numeric, txcarr_s numeric, rxfram_s numeric, rxfifo_s numeric, txfifo_s numeric );

-- network device nfs client
-- linux format: hostname;interval;timestamp;call/s;retrans/s;read/s;write/s;access/s;getatt/s
drop table if exists sar_network_nfs_client;
create table sar_network_nfs_client ( hostname varchar(100), interval int, timestamp timestamp, call_s numeric, retrans_s numeric, read_s numeric, write_s numeric, access_s numeric, getatt_s numeric );

-- network device nfs server
-- linux format: hostname;interval;timestamp;scall/s;badcall/s;packet/s;udp/s;tcp/s;hit/s;miss/s;sread/s;swrite/s;saccess/s;sgetatt/s
drop table if exists sar_network_nfs_server;
create table sar_network_nfs_server ( hostname varchar(100), interval int, timestamp timestamp, scall_s numeric, badcall_s numeric, packet_s numeric, udp_s numeric, tcp_s numeric, hit_s numeric, miss_s numeric, sread_s numeric, swrite_s numeric, access_s numeric, sgetatt_s numeric );

-- network device sockets
-- linux format: hostname;interval;timestamp;totsck;tcpsck;udpsck;rawsck;ip-frag;tcp-tw
drop table if exists sar_network_sockets;
create table sar_network_sockets ( hostname varchar(100), interval int, timestamp timestamp, totsck numeric, tcpsck numeric, udpsck numeric, rawsck numeric, ip_frag numeric, tcp_tw numeric );

-- paging
-- linux format: hostname;interval;timestamp;pgpgin/s;pgpgout/s;fault/s;majflt/s;pgfree/s;pgscank/s;pgscand/s;pgsteal/s;%vmeff
drop table if exists sar_paging;
create table sar_paging ( hostname varchar(100), interval int, timestamp timestamp, pgpgin_s numeric, pgpgout_s numeric, fault_s numeric, majflt_s numeric, pgfree_s numeric, pgscank_s numeric, pgscand_s numeric, pgsteal_s numeric, perc_vmeff numeric );

-- runqueue
-- linux format: hostname;interval;timestamp;runq-sz;plist-sz;ldavg-1;ldavg-5;ldavg-15;blocked
drop table if exists sar_runqueue;
create table sar_runqueue ( hostname varchar(100), interval int, timestamp timestamp, runq_sz numeric, plist_sz numeric, ldavg_1 numeric, ldavg_5 numeric, ldavg_15 numeric, blocked numeric );

-- swap statistics
-- linux format: hostname;interval;timestamp;pswpin/s;pswpout/s
drop table if exists sar_swap_statistics;
create table sar_swap_statistics ( hostname varchar(100), interval int, timestamp timestamp, pswpin_s numeric, pswpout_s numeric );

-- swap utilization
-- linux format: hostname;interval;timestamp;kbswpfree;kbswpused;%swpused;kbswpcad;%swpcad
drop table if exists sar_swap_utilization;
create table sar_swap_utilization ( hostname varchar(100), interval int, timestamp timestamp, kbswpfree numeric, kbswpused numeric, perc_swpused numeric, kbswpcad numeric, perc_swpcad numeric );

-- task statistics
-- linux format: hostname;interval;timestamp;proc/s;cswch/s
drop table if exists sar_task_statistics;
create table sar_task_statistics ( hostname varchar(100), interval int, timestamp timestamp, proc_s numeric, cswch_s numeric );

-- serial
-- linux format: hostname;interval;timestamp;TTY;rcvin/s;txmtin/s;framerr/s;prtyerr/s;brk/s;ovrun/s
-- however, the generated csv file seems to include a tty number for every statistic ?!:
-- hostname;593;2018-08-31 00:10:01;0;0.00;0;0.00;0;0.00;0;0.00;0;0.00;0;0.00
drop table if exists sar_serial;
create table sar_serial ( hostname varchar(100), interval int, timestamp timestamp, tty int, rcvin_s numeric, tty_1 int, txmtin_s numeric, tty_2 int, framerr_s numeric, tty_3 int, prtyerr_s numeric, tty_4 int, brk_s numeric, tty_5 int, ovrun_s numeric );
