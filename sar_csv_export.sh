#
# sar exporter by frits hoogland
#
SA_DIR=/var/log/sa
REMOVE_CSV=true
#
# check if sadf is present
if [ -z "$(which sadf 2>/dev/null)" ]; then
  echo "The executable sadf is not in the path/can not be found."
  echo "sadf is part of the sysstat package that contains sar and sets up creating sar archives, please install that."
  exit 1
fi
# get activities from current file
SADF_H=$(sadf -H | grep ^[0-9]*:)
echo "$SADF_H" > sar_activities.txt
#
TARBALL_FILES="sar_activities.txt"
for ACTIVITY in $(echo "$SADF_H" | awk '{ print $2 }'); do
  echo "Processing $ACTIVITY"
  if [ $ACTIVITY = "A_MEMORY" ]; then
    > sar_A_MEMORY_util.csv
    > sar_A_MEMORY_swap.csv
    TARBALL_FILES=$(echo "$TARBALL_FILES sar_${ACTIVITY}_util.csv sar_${ACTIVITY}_swap.csv")
  elif [ $ACTIVITY = "A_CPU" ]; then
    > sar_${ACTIVITY}.csv
    > sar_restart.csv
    TARBALL_FILES=$(echo "$TARBALL_FILES sar_$ACTIVITY.csv")
  else
    > sar_${ACTIVITY}.csv
    TARBALL_FILES=$(echo "$TARBALL_FILES sar_$ACTIVITY.csv")
  fi
  for FILE in $(ls ${SA_DIR}/sa[0-9]*); do
    case $ACTIVITY in
      A_CPU)
        sadf -dt -P ALL $FILE -- -u | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        sadf -dt $FILE -- -u >> sar_restart.csv
        ;;
      A_PCSW)
        sadf -dt $FILE -- -w | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        ;;
      A_SWAP)
        sadf -dt $FILE -- -W | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        ;;
      A_PAGE)
        sadf -dt $FILE -- -B | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        ;;
      A_IO)
        sadf -dt $FILE -- -b | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        ;;
      A_MEMORY)
        sadf -dt $FILE -- -r | awk '/^#/,EOF' >> sar_${ACTIVITY}_util.csv
        sadf -dt $FILE -- -S | awk '/^#/,EOF' >> sar_${ACTIVITY}_swap.csv
        ;;
      A_HUGE)
        sadf -dt $FILE -- -H | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        ;;
      A_KTABLES)
        sadf -dt $FILE -- -v | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        ;;
      A_QUEUE)
        sadf -dt $FILE -- -q | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        ;;
      A_DISK)
        sadf -dt $FILE -- -d | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        ;;
      A_NET_DEV)
        sadf -dt $FILE -- -n DEV | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        ;;
      A_NET_EDEV)
        sadf -dt $FILE -- -n EDEV | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        ;;
      A_NET_NFS)
        sadf -dt $FILE -- -n NFS | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        ;;
      A_NET_NFSD)
        sadf -dt $FILE -- -n NFSD | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        ;;
      A_NET_SOCK)
        sadf -dt $FILE -- -n SOCK | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        ;;
      A_SERIAL)
        sadf -dt $FILE -- -y | awk '/^#/,EOF' >> sar_${ACTIVITY}.csv
        ;;
      *)
        echo "Activity not implemented: $ACTIVITY"
        ;;
    esac
  done
  # find restarts. two issues here:
  # - I assume A_CPU is used, sar_restart.csv is generated during A_CPU. I think this is a safe bet.
  # - Restarts on linux a visible as LINUX_RESTART, this is probably different on non-linux systems :-)
  if [ $ACTIVITY = "A_CPU" ]; then
    sed -i '/LINUX-RESTART/!d' sar_restart.csv
    TARBALL_FILES=$(echo "$TARBALL_FILES sar_restart.csv")
  fi
  # remove csv headers from csv and create header file
  # also remove LINUX-RESTART
  if [ $ACTIVITY = "A_MEMORY" ]; then
    for EXTRA in util swap; do
      grep ^# sar_${ACTIVITY}_${EXTRA}.csv | uniq > sar_${ACTIVITY}_${EXTRA}.header
      TARBALL_FILES=$(echo "$TARBALL_FILES sar_${ACTIVITY}_${EXTRA}.header")
      sed -i '/^#/d' sar_${ACTIVITY}_${EXTRA}.csv
      sed -i '/LINUX-RESTART/d' sar_${ACTIVITY}_${EXTRA}.csv
    done
  else
    grep ^# sar_${ACTIVITY}.csv | uniq > sar_${ACTIVITY}.header
    TARBALL_FILES=$(echo "$TARBALL_FILES sar_${ACTIVITY}.header")
    sed -i '/^#/d' sar_${ACTIVITY}.csv
    sed -i '/LINUX-RESTART/d' sar_${ACTIVITY}.csv
  fi
done
TARBALL_NAME=$(hostname)_$(date +%Y%m%d%H%M)_sar_csv.tgz
tar czf $TARBALL_NAME $TARBALL_FILES
echo "Files are in $TARBALL_NAME"
[ $REMOVE_CSV = "true" ] && rm $TARBALL_FILES
